package ru.ilnitsky.nsk.java.screenloupe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

/**
 * Экранная лупа на основе Swing
 * Created by Mike on 22.04.2017.
 */
public class ScreenLoupe {
    private class LoupePanel extends JPanel {
        private final Cursor goalCursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
        private final Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);

        private Dimension screenSize;
        private BufferedImage image;
        private boolean isStarted;
        private boolean isDragged;
        private boolean isChanged;

        private Point loupePosition;
        private Point lastPosition;
        private Rectangle screenWindow;

        private int zoom = 4;
        private int step = 1;
        private int longStep = 16;

        private int panelWidth;
        private int panelHeight;
        private int onScreenWidth;
        private int onScreenHeight;
        private int halfWidth;
        private int halfHeight;

        private final Timer timer = new Timer(40, l -> {
            if (isStarted) {
                captureImage();
                repaint();
            }
        });

        private int setZoom(int zoom) {
            int lastZoom = this.zoom;
            if (zoom > 16) {
                this.zoom = 16;
            } else if (zoom < 2) {
                this.zoom = 2;
            } else {
                this.zoom = zoom;
            }
            if (zoom != lastZoom) {
                isChanged = true;
            }
            return this.zoom;
        }

        private int setStep(int step) {
            if (step >= longStep) {
                this.step = longStep - 1;
            } else if (step < 1) {
                this.step = 1;
            } else {
                this.step = step;
            }
            return this.step;
        }

        private int setLongStep(int longStep) {
            int max = Math.min(screenSize.height, screenSize.width) / 4;
            if (longStep > max) {
                this.longStep = max;
            } else if (longStep <= step) {
                this.longStep = step + 1;
            } else {
                this.longStep = longStep;
            }
            return this.longStep;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            if (image != null) {
                int px = getWidth();
                int py = getHeight();
                int ix = image.getWidth();
                int iy = image.getHeight();

                float panelCoefficient = px / (float) py;
                float imageCoefficient = ix / (float) iy;
                float d = panelCoefficient - imageCoefficient;

                if (Math.abs(d) < 1e-3) {
                    g.drawImage(image, 0, 0, px, py, this);
                } else if (d < 0) {
                    int y = (int) (px / imageCoefficient);
                    g.drawImage(image, 0, 0, px, y, this);
                } else {
                    int x = (int) (py * imageCoefficient);
                    g.drawImage(image, 0, 0, x, py, this);
                }
            }
        }

        void captureImage() {
            if (isStarted) {
                if (isChanged || lastPosition == null || !lastPosition.equals(loupePosition)
                        || panelWidth != getWidth() || panelHeight != getHeight()) {

                    panelWidth = getWidth();
                    panelHeight = getHeight();
                    onScreenWidth = panelWidth / zoom;
                    onScreenHeight = panelHeight / zoom;
                    halfWidth = onScreenWidth / 2;
                    halfHeight = onScreenHeight / 2;

                    if (loupePosition.x < halfWidth) {
                        loupePosition.x = halfWidth;
                    } else if (screenSize.width - loupePosition.x < halfWidth) {
                        loupePosition.x = screenSize.width - halfWidth;
                    }
                    if (loupePosition.y < halfHeight) {
                        loupePosition.y = halfHeight;
                    } else if (screenSize.height - loupePosition.y < halfHeight) {
                        loupePosition.y = screenSize.height - halfHeight;
                    }

                    lastPosition = new Point(loupePosition.x, loupePosition.y);
                    isChanged = false;

                    screenWindow = new Rectangle(loupePosition.x - halfWidth, loupePosition.y - halfHeight, onScreenWidth, onScreenHeight);
                }

                try {
                    Robot robot = new Robot();
                    image = robot.createScreenCapture(screenWindow);
                } catch (AWTException ex) {
                    ex.printStackTrace();
                }
            }
        }

        LoupePanel() {
            screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            isChanged = true;

            addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {

                        isStarted = true;
                        isDragged = true;
                        timer.start();
                        loupePosition = e.getLocationOnScreen();

                        setCursor(goalCursor);

                        captureImage();
                        repaint();
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        isDragged = false;

                        setCursor(defaultCursor);

                        captureImage();
                        repaint();
                    }
                }
            });

            addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    super.mouseDragged(e);

                    if (isDragged) {
                        loupePosition = e.getLocationOnScreen();

                        captureImage();
                        repaint();
                    }
                }
            });

        }
    }

    private JFrame frame = new JFrame("NskScreenLoupe - экранная лупа");
    private LoupePanel loupePanel = new LoupePanel();
    private JMenuBar menuBar = new JMenuBar();

    private ScreenLoupe() {
        Image image = new ImageIcon(getClass().getResource("/ru/ilnitsky/nsk/java/screenloupe/NskScreenLoupe.png")).getImage();
        frame.setIconImage(image);

        frame.setAlwaysOnTop(true);
        frame.setSize(400, 320);
        frame.setLocation(loupePanel.screenSize.width - frame.getWidth(), loupePanel.screenSize.height - frame.getHeight());
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exitDialog();
            }
        });

        initMenuBar();
        frame.add(loupePanel, BorderLayout.CENTER);

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);

                if (loupePanel.isStarted) {
                    int step;
                    if (e.isControlDown()) {
                        step = loupePanel.longStep;
                    } else {
                        step = loupePanel.step;
                    }
                    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        loupePanel.loupePosition.x += step;
                    } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                        loupePanel.loupePosition.x -= step;
                    }
                    if (e.getKeyCode() == KeyEvent.VK_UP) {
                        loupePanel.loupePosition.y -= step;
                    } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                        loupePanel.loupePosition.y += step;
                    }
                }
            }
        });
    }

    private void setVisible() {
        frame.setVisible(true);
    }

    private void initMenuBar() {
        JMenu menuFile = new JMenu("Файл");
        JMenuItem itemOff = new JMenuItem("Стоп(выключить)");
        itemOff.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        itemOff.addActionListener(e -> loupePanel.isStarted = false);
        menuFile.add(itemOff);
        JMenuItem itemExit = new JMenuItem("Выход");
        itemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        itemExit.addActionListener(e -> exitDialog());
        menuFile.add(itemExit);

        JMenu menuZoom = new JMenu("Масштаб");
        ButtonGroup groupZoom = new ButtonGroup();
        JRadioButtonMenuItem[] itemZoom = new JRadioButtonMenuItem[15];
        for (int i = 0; i < itemZoom.length; i++) {
            int zoom = i + 2;
            itemZoom[i] = new JRadioButtonMenuItem("x " + zoom + " (" + (zoom * 100) + "%)");
            itemZoom[i].addActionListener(e -> loupePanel.setZoom(zoom));
            menuZoom.add(itemZoom[i]);
            groupZoom.add(itemZoom[i]);
        }

        JMenu menuStep = new JMenu("Шаг");
        ButtonGroup groupStep = new ButtonGroup();
        JRadioButtonMenuItem[] itemStep = new JRadioButtonMenuItem[15];
        for (int i = 0; i < itemStep.length; i++) {
            int step = i + 1;
            itemStep[i] = new JRadioButtonMenuItem(Integer.toString(step));
            itemStep[i].addActionListener(e -> loupePanel.setStep(step));
            menuStep.add(itemStep[i]);
            groupStep.add(itemStep[i]);
        }

        JMenu menuLongStep = new JMenu("Длинный шаг");
        ButtonGroup groupLongStep = new ButtonGroup();
        JRadioButtonMenuItem[] itemLongStep = new JRadioButtonMenuItem[6];
        for (int i = 0; i < itemLongStep.length; i++) {
            int step = 16 * (int) Math.pow(2, i);
            itemLongStep[i] = new JRadioButtonMenuItem(Integer.toString(step));
            itemLongStep[i].addActionListener(e -> loupePanel.setLongStep(step));
            menuLongStep.add(itemLongStep[i]);
            groupLongStep.add(itemLongStep[i]);
        }

        JMenu menuAbout = new JMenu("О программе");
        JMenuItem itemAbout = new JMenuItem("О программе");
        itemAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        itemAbout.addActionListener(e ->
                JOptionPane.showMessageDialog(frame,
                        new String[]{"NskScreenLoupe",
                                "Экранная лупа",
                                "Графический интерфейс на основе Swing",
                                "М.Ильницкий, Новосибирск, 2017"},
                        "О программе",
                        JOptionPane.INFORMATION_MESSAGE)
        );
        menuAbout.add(itemAbout);

        menuBar.add(menuFile);
        menuBar.add(menuZoom);
        menuBar.add(menuStep);
        menuBar.add(menuLongStep);
        menuBar.add(menuAbout);

        frame.setJMenuBar(menuBar);

        itemZoom[0].doClick();
        itemStep[0].doClick();
        itemLongStep[0].doClick();
    }

    private void exitDialog() {
        try {
            Object options[] = {"Да", "Нет"};
            int result = JOptionPane.showOptionDialog(frame,
                    "Завершить программу?",
                    "Подтверждение завершения",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            if (result == JOptionPane.OK_OPTION) {
                System.exit(0);
            }
        } catch (Exception exception) {
            exception.getStackTrace();
        }
    }

    public static void main(String[] args) {
        ScreenLoupe screenLoupe = new ScreenLoupe();
        screenLoupe.setVisible();
    }
}
